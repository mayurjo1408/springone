package com.springpractice.project1;

import org.springframework.stereotype.Component;

@Component
public class ResultEnhancer implements IRoundOffMaker {

	@Override
	public double getRoundOffValue(double result) {
		return Math.abs(result);
	}

}
