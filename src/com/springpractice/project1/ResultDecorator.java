package com.springpractice.project1;

import org.springframework.stereotype.Component;

@Component
public class ResultDecorator implements IRoundOffMaker {

	@Override
	public double getRoundOffValue(double result) {
		return Math.round(result);
	}

}
