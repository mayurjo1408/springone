package com.springpractice.project1;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main2 {

	public static void main(String[] args) {
		// Read Spring config file
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		// Get the bean from Spring container
		ICalculator ic1 = context.getBean("addition", ICalculator.class);

		ICalculator ic2 = context.getBean("addition", ICalculator.class);
		// --

		// Check result
		System.out.println("ic1: " + ic1);
		System.out.println("ic2: " + ic2);

		System.out.println("Is Equals?: " + (ic1 == ic2));

		// Close the context
		context.close();
	}

}
