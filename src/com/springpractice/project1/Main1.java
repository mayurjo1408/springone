package com.springpractice.project1;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main1 {

	public static void main(String[] args) {
		System.out.println("Jay Ganesh!");

		// Read Spring config file
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		// Get the bean from Spring container
		ICalculator ic = context.getBean("addition", ICalculator.class);

		// Call the method of the bean
		double dblResult = ic.getResult(12.39, 17.12);

		System.out.println("Result: " + dblResult);

		// Call another method
		System.out.println("Final Result: " + ic.getFinalValue(dblResult));

		// Close the context
		context.close();
	}

}
