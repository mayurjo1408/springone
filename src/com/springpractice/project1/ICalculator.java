package com.springpractice.project1;

public interface ICalculator {
	public double getResult(double num1, double num2);

	public double getFinalValue(double result);
}
