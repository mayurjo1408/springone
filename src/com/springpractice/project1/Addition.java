package com.springpractice.project1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
public class Addition implements ICalculator {

	// Field injection
	@Autowired
	@Qualifier("resultEnhancer")
	private IRoundOffMaker rom;

	// Default constructor
	public Addition() {
		System.out.println("Inside Addition - Addition()");
	}

	/*
	 * // Constructor injection
	 * 
	 * @Autowired public void setRom(IRoundOffMaker rom) {
	 * System.out.println("Inside Addition - setRom()"); this.rom = rom; } //--
	 * 
	 * @Autowired public void testMethod(IRoundOffMaker rom) {
	 * System.out.println("Inside Addition - testMethod()"); this.rom = rom; }
	 * 
	 */
	// Constructor injection
	/*
	 * @Autowired public Addition(IRoundOffMaker rom) { super(); this.rom = rom; }
	 * 
	 */

	@Override
	public double getResult(double num1, double num2) {
		return num1 + num2;
	}

	@Override
	public double getFinalValue(double result) {
		return rom.getRoundOffValue(result);
	}

}
