package com.springpractice.project1;

public interface IRoundOffMaker {
	public double getRoundOffValue(double result);
}
